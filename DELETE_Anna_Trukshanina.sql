/* Remove a previously inserted film from the inventory and all corresponding rental records */
  
delete from inventory
where film_id in (select film_id from film where title =  'The Twilight Saga: New Moon');

delete from rental
where inventory_id in 
(select inventory_id from inventory where film_id in 
 (select film_id from film where title =  'The Twilight Saga: New Moon'));


/* Remove any records related to you (as a customer) from all tables 
except "Customer" and "Inventory" */

delete from payment where customer_id in
(select customer_id from customer where first_name = 'Anna' and last_name = 'Trukshanina' and email = 'trukhanina.anna@student.ehu');

delete from rental where customer_id in
(select customer_id from customer where first_name = 'Anna' and last_name = 'Trukshanina' and email = 'trukhanina.anna@student.ehu');
