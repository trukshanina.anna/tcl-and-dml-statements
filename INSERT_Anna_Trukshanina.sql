/*Choose one of your favorite films and add it to the "film" table. 
Fill in rental rates with 4.99 and rental durations with 2 weeks.*/

INSERT INTO film (title, rental_rate, rental_duration, language_id)
VALUES ('Twilight', 4.99, 2, 1);

select * from film
where title = 'Twilight';

/*Add the actors who play leading roles in your favorite film to the 
"actor" and "film_actor" tables (three or more actors in total).*/

INSERT INTO actor ( first_name, last_name)
VALUES
  ( 'Kristen', 'Stewart'),
  ( 'Robert', 'Pattinson'),
  ( 'Nikki', 'Reed');

WITH ActorIds AS (
    SELECT actor_id
    FROM actor
    WHERE (first_name, last_name) IN (('Kristen', 'Stewart'), ('Robert', 'Pattinson'), ('Nikki', 'Reed'))
)

INSERT INTO film_actor (actor_id, film_id)
SELECT ai.actor_id, f.film_id
FROM ActorIds ai
JOIN film f ON f.title = 'Twilight'

--проверка
SELECT a.first_name, a.last_name
FROM actor a
JOIN film_actor fa ON a.actor_id = fa.actor_id
JOIN film f ON fa.film_id = f.film_id
WHERE f.title = 'Twilight';

/*Add your favorite movies to any store's inventory.*/

INSERT INTO film (title, rental_rate, rental_duration, language_id)
VALUES
  ('The Twilight Saga: Breaking Dawn', 4.99, 2, 1),
  ('The Twilight Saga: Eclipse', 4.99, 2, 1),
  ('The Twilight Saga: New Moon', 4.99, 2, 1);
  
WITH FilmIds AS (
    SELECT film_id
    FROM film
    WHERE title IN (
        'The Twilight Saga: Breaking Dawn',
        'The Twilight Saga: Eclipse',
        'The Twilight Saga: New Moon'
    )
)

INSERT INTO inventory (film_id, store_id)
SELECT fi.film_id, 1
FROM FilmIds fi

-- проверка
SELECT i.inventory_id, f.title, i.store_id
FROM inventory i
JOIN film f using (film_id)
WHERE
    f.title IN (
        'The Twilight Saga: Breaking Dawn',
        'The Twilight Saga: Eclipse',
        'The Twilight Saga: New Moon'
    )
	AND i.store_id = 1

