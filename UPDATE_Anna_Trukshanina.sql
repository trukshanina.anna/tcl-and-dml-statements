/* Alter the rental duration and rental rates of the film you 
inserted before to three weeks and 9.99, respectively.*/

update film
set
  rental_duration = 3,
  rental_rate = 9.99
where title in ('The Twilight Saga: Breaking Dawn', 'The Twilight Saga: Eclipse', 'The Twilight Saga: New Moon');
  
  select * from film
where title in ('The Twilight Saga: Breaking Dawn', 'The Twilight Saga: Eclipse', 'The Twilight Saga: New Moon');

/*Alter any existing customer in the database with at least 10 rental and 10 payment 
records. Change their personal data to yours (first name, last name, address, etc.).
You can use any existing address from the "address" table. Please do not perform 
any updates on the "address" table, as this can impact multiple records with the same address.*/

with CustomerToUpdate as (
  select c.customer_id
  from customer c
    join rental r on c.customer_id = r.customer_id
    join payment p on c.customer_id = p.customer_id
  group by c.customer_id
  having count(distinct r.rental_id) >= 10
    and count(distinct p.payment_id) >= 10
  limit 1
)

update customer
set
  first_name = 'Anna',
  last_name = 'Trukshanina',
  email = 'trukhanina.anna@student.ehu',
  address_id = (select address_id from address order by random() limit 1)
where
  customer_id in (select customer_id from CustomerToUpdate);
 
 
 select customer_id, first_name, last_name, email, address_id
 from customer
 where first_name = 'Anna' and last_name = 'Trukshanina' and email = 'trukhanina.anna@student.ehu';
 
 
/* Change the customer's create_date value to current_date */

 update customer
 set
 create_date = CURRENT_DATE
 where first_name = 'Anna' and last_name = 'Trukshanina' and email = 'trukhanina.anna@student.ehu';


 select customer_id, first_name, last_name, email, create_date
 from customer
 where first_name = 'Anna' and last_name = 'Trukshanina' and email = 'trukhanina.anna@student.ehu';
  
  
